package com.gitlab.gatesumdotnet.runner;

import com.google.common.collect.Lists;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class SparkExampleMain {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setAppName("Word Count")
//                .setMaster("local");
                .setMaster("spark://localhost:7077");
        conf.validateSettings();
        JavaSparkContext sparkContext = new JavaSparkContext(conf);
        sparkContext.addJar("ExampleRunner/build/libs/ExampleRunner-1.0-SNAPSHOT.jar");
        JavaRDD<String> parallelize = sparkContext.parallelize(Lists.newArrayList("1", "2", "3", "4", "5"));
        JavaRDD<String> validNumberString = parallelize.filter(string -> !string.isEmpty());
        JavaRDD<Integer> numbers = validNumberString.map(Integer::valueOf);
        int finalSum = numbers.reduce((x, y) -> x + y);

        System.out.println("Final sum is: " + finalSum);
        sparkContext.close();
    }
}
