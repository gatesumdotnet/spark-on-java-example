#!/usr/bin/env bash

docker stop spark-worker-1
docker stop spark-worker-2
docker stop spark-worker-3
docker stop spark-master
