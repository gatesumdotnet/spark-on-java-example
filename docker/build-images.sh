#!/usr/bin/env bash

set -e

SPARK_VERSION=2.3.1

echo '************'
echo '** Master **'
echo '************'
docker build --build-arg spark_version=${SPARK_VERSION} -t spark-master:${SPARK_VERSION} ./spark-master
echo '************'
echo '** Submit **'
echo '************'
docker build --build-arg spark_version=${SPARK_VERSION} -t spark-submit:${SPARK_VERSION} ./spark-submit
echo '************'
echo '** Worker **'
echo '************'
docker build --build-arg spark_version=${SPARK_VERSION} -t spark-worker:${SPARK_VERSION} ./spark-worker
