#!/usr/bin/env bash

SPARK_VERSION=2.3.1

echo '************'
echo '**  Base  **'
echo '************'
docker build --build-arg spark_version=${SPARK_VERSION} -t spark-base:${SPARK_VERSION} ./base
