MAKEFLAGS += --silent

DOCKER_DIR := docker

## help: Prints a list of available build targets.
help:
	echo "Usage: make <OPTIONS> ... <TARGETS>"
	echo ""
	echo "Available targets are:"
	echo ''
	sed -n 's/^##//p' ${PWD}/Makefile | column -t -s ':' | sed -e 's/^/ /'
	echo
	echo "Targets run by default are: `sed -n 's/^all: //p' ./Makefile | sed -e 's/ /, /g' | sed -e 's/\(.*\), /\1, and /'`"

all: init-docker-images docker-compose-up

## init-docker-images: initializes docker images on local machine.
init-docker-images:
	cd ${DOCKER_DIR} && ./build-images.sh

## docker-compose-up: starts up spark docker cluster
docker-compose-up:
	cd ${DOCKER_DIR} && docker-compose up

## docker-force-down: manually stops all running containers
docker-force-down:
	cd ${DOCKER_DIR} && ./docker-force-stop.sh

.PHONY: init-docker-images docker-compose-up docker-force-down
